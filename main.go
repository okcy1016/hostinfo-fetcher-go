// +build windows
package main

import (
	"fmt"
	"encoding/json"
	"io/ioutil"
	"time"
)

func main() {
	
	for {
		hostInfoStruct := FetchHostInfo()
		hostInfoBytes, _ := json.Marshal(hostInfoStruct)
		fmt.Println(string(hostInfoBytes))

		// write host info into file
		err := ioutil.WriteFile("hostinfo.json", hostInfoBytes, 0644)
		if err != nil {
			fmt.Println("Failed to write host info into file.", err)
		}
		time.Sleep(30 * time.Second);
	}
}
