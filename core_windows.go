// +build windows
package main

import (
	"fmt"
	// "github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/net"
	log "github.com/sirupsen/logrus"
	"os/exec"
	"os"
	"strings"
	"syscall"
	// "time"
)

type interfaceStat struct {
	InterfaceName string
	InterfaceAddr string
}

type hostInfo struct {
	CurrentUser        string
	CpuPrecent         string
	TotalMemory        string
	UsedMemoryPercent  string
	TotalDiskVolumn    string
	UsedDiskVolumn     string
	InterfacesStat     []interfaceStat
	SharedPrintersName []string
}

func FetchHostInfo() hostInfo {
	// Current user name
	currentUserNameStr := os.Getenv("username")

	// // CPU status
	// cpuPercentIntSlice, err := cpu.Percent(0*time.Second, true)
	// if err != nil {
	// 	log.Error("Failed to get cpu info: ", err)
	// }
	// cpuPercentString := fmt.Sprintf("%.1f%%", cpuPercentIntSlice[0])

	// Memory status
	vm, err := mem.VirtualMemory()
	if err != nil {
		log.Error("Failed to get memory info: ", err)
	}
	totalMemString := fmt.Sprintf("%.1fGB", float64(vm.Total)/(1024*1024*1000))
	usedMemPercentString := fmt.Sprintf("%.0f%%", vm.UsedPercent)

	// Disk status
	diskUsageStru, err := disk.Usage("C:/")
	if err != nil {
		log.Error("Failed to get disk info: ", err)
	}
	cDiskUsedDiskVolumnString := fmt.Sprintf("%.1fGB", float64(diskUsageStru.Used)/(1024*1024*1000))
	cDiskTotalDiskVolumnString := fmt.Sprintf("%.1fGB", float64(diskUsageStru.Total)/(1024*1024*1000))

	// Network status
	var interfaceStatSlice []interfaceStat
	netStatStruSlice, err := net.Interfaces()
	if err != nil {
		log.Error("Failed to get network info: ", err)
	}
	for _, netStatStru := range netStatStruSlice {
		// Discard non-active interface
		if netStatStru.Flags[0] != "up" || len(netStatStru.Addrs) < 2 {
			continue
		}
		// Discard reserved interface
		if netStatStru.Addrs[1].Addr[:7] == "169.254" {
			continue
		}
		// Discard loopback pseudo interface
		if netStatStru.Flags[1] == "loopback" {
			continue
		}
		// Discard pointtopoint interface
		if netStatStru.Flags[1] == "pointtopoint" {
			continue
		}
		interfaceStatSlice = append(interfaceStatSlice, interfaceStat{
			InterfaceName: netStatStru.Name,
			InterfaceAddr: netStatStru.Addrs[1].Addr[:len(netStatStru.Addrs[1].Addr)-3],
		})
	}

	// Get printer names
	getPrinterNameCmd := exec.Command("cmd.exe", "/c", "wmic printer get name")
	// Don't spawn a cmd window
	getPrinterNameCmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	rawPrinterNameByte, err := getPrinterNameCmd.Output()
	if err != nil {
		log.Error("Failed to get printer info: ", err)
	}
	rawPrinterNameString := string(rawPrinterNameByte)
	rawPrinterNameStringSlice := strings.Split(rawPrinterNameString, "\r\r\n")
	// Only get physics machine's name
	var printerNameStringSlice []string
	for _, printNameString := range rawPrinterNameStringSlice {
		printNameString = strings.TrimSpace(printNameString)
		if printNameString == "" || printNameString[:4] == "Name" {
			continue
		}
		printerNameStringSlice = append(printerNameStringSlice, printNameString)
	}

	hostInfoStruct := hostInfo{
		CurrentUser:        currentUserNameStr,
		// CpuPrecent:         cpuPercentString,
		TotalMemory:        totalMemString,
		UsedMemoryPercent:  usedMemPercentString,
		TotalDiskVolumn:    cDiskTotalDiskVolumnString,
		UsedDiskVolumn:     cDiskUsedDiskVolumnString,
		InterfacesStat:     interfaceStatSlice,
		SharedPrintersName: printerNameStringSlice,
	}
	return hostInfoStruct
}
